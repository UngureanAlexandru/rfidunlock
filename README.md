# RfidUnlock
Pc/Laptop unlock using an arduino rfid. There are 2 .sh scripts for Linux. getStatus.sh gets the gnome screensaver status (not working under root, only under a user) and loginService.sh sends the status to an arduino micro board that can simulate key presses.

The arduino receive 1 or 0 from the .sh script and it should receive 0 if the screen is unlocked and 1 if it is locked. If the arduino gets a 1 and the user use the tag, the arduino will type the password.

To do:
	- make the getStatus.sh run as root
