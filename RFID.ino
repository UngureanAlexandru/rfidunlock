#include <Keyboard.h>

#include <SPI.h>
#include <MFRC522.h>

#define RST_PIN         9          // Configurable, see typical pin layout above
#define SS_PIN          10         // SDA pin

MFRC522 mfrc522(SS_PIN, RST_PIN);

String password = "Your password"; // Show be stored in eprom and not hardcoded
byte accessUID[7] = {0x04, 0xAE, 0xC6, 0x92, 0xC8, 0x48, 0x80};

byte block = 4;
byte len = 18;
MFRC522::StatusCode status;
MFRC522::MIFARE_Key key;
byte dataBuffer[18];
byte uid[7];

int uidAccess = 0;

int stringToDec(String input);

void setup()
{
  Serial.begin(9600);
  Keyboard.begin();

  //while (!Serial);

  SPI.begin();
  mfrc522.PCD_Init();
  mfrc522.PCD_DumpVersionToSerial();
}

void loop()
{
    if (Serial.available() == 0)
    {
        return;
    }
    char input = Serial.read();

     if (input != '1')
     {
        return;
     }
    
    for (int i = 0; i < 6; i++)
    {
        key.keyByte[i] = 0xFF;
    }

    if (!mfrc522.PICC_IsNewCardPresent())
    {
        return;
    }

    Serial.println("New tag");

    if (!mfrc522.PICC_ReadCardSerial())
    {
        return;
    }

    mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid));

  ///////////////////////// UID access ///////////////////////

    uidAccess = 1;

    for (int i = 0; i < 7; i++)
    {
        uid[i] = mfrc522.uid.uidByte[i];

        if (uid[i] != accessUID[i])
        {
            uidAccess = 0;
        }
    }

//  for (int i = 0; i < 7; i++)
//  {
//    Serial.print(uid[i]);
//  }
//  Serial.println("");
//  for (int i = 0; i < 7; i++)
//  {
//    Serial.print(accessUID[i]);
//  }
//  Serial.println("");

  ////////////////////////////////////////////////////////////

    int tagData = 0;

    if (!uidAccess)
    {
        status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 4, &key, &(mfrc522.uid));
        if (status != MFRC522::STATUS_OK)
        {
            Serial.print(F("Authentication failed: "));
            Serial.println(mfrc522.GetStatusCodeName(status));
            
            return;
        }

        len = 18;

        status = mfrc522.MIFARE_Read(block, dataBuffer, &len);
        if (status != MFRC522::STATUS_OK)
        {
            Serial.print(F("Reading failed: "));
            Serial.println(mfrc522.GetStatusCodeName(status));

            //mfrc522.PCD_Reset();
            //mfrc522.PCD_Init();

            return;
        }

        for (uint8_t i = 0; i < 16; i++)
        {
            if (dataBuffer[i] != 32 && dataBuffer[i] != 10 && dataBuffer[i] != 13)
            {
                tagData += dataBuffer[i];
        //      Serial.print("|");
        //      Serial.print((char)dataBuffer[i]);
        //      Serial.println("|");
            }
        }
        Serial.println("");

        delay(500);

        mfrc522.PICC_HaltA();
        mfrc522.PCD_StopCrypto1();

        //Serial.println((int)tagData);
        //Serial.println((int)stringToDec(password));
    }
    
    if (stringToDec(password) == tagData || uidAccess == 1)
    {
        //Serial.println("Acces granted!");
        Keyboard.println(password);
        Keyboard.write(13);
        Keyboard.write(10);
        delay(1000);

        if (uidAccess)
        {
            uidAccess = 0;
        }
    }
    else
    {
    //Serial.println("Acces denied!");
    }
    //Serial.println("");
    
}


int stringToDec(String input)
{
  int result = 0;

  for (int i = 0; i < input.length(); i++)
  {
    result += byte(input[i]);
  }
  return result;
}

